<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Register_Post_Taxonomies {
	public function __construct() {
		add_action( 'init', array( $this, 'taxonomies_init' ) );
	}

	public function taxonomies_init() {
		$taxonomies = apply_filters( 'stm_post_taxonomies', true );
		foreach ( $taxonomies as $taxonomy => $taxonomy_args ) {
			register_taxonomy( $taxonomy, $taxonomy_args['post_type'], $taxonomy_args['args'] );
		}
	}
}

new Register_Post_Taxonomies();
