<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class Register_Post_Types {
	public function __construct() {
		add_action( 'init', array( $this, 'post_types_init' ) );
	}

	public function post_types_init() {
		$post_types = apply_filters( 'stm_post_types', true );

		foreach ( $post_types as $post_type => $post_type_info ) {
			$add_args = ( ! empty( $post_type_info['args'] ) ) ? $post_type_info['args'] : array();
			$args     = $this->post_type_args(
				$this->post_types_labels(
					$post_type_info['single'],
					$post_type_info['plural']
				),
				$post_type,
				$add_args
			);

			register_post_type( $post_type, $args );
		}
	}

	public function post_types_labels( $singular, $plural ) {
		$admin_bar_name = ( ! empty( $admin_bar_name ) ) ? $admin_bar_name : $plural;
		return array(
			'name'               => _x( sprintf( '%s', $plural ), 'post type general name', 'starter-core' ),
			'singular_name'      => _x( sprintf( '%s', $singular ), 'post type singular name', 'starter-core' ),
			'menu_name'          => _x( sprintf( '%s', $plural ), 'admin menu', 'starter-core' ),
			'name_admin_bar'     => sprintf( _x( '%s', 'Admin bar ' . $singular . ' name', 'starter-core' ), $admin_bar_name ),
			'add_new_item'       => sprintf( __( 'Add New %s', 'starter-core' ), $singular ),
			'new_item'           => sprintf( __( 'New %s', 'starter-core' ), $singular ),
			'edit_item'          => sprintf( __( 'Edit %s', 'starter-core' ), $singular ),
			'view_item'          => sprintf( __( 'View %s', 'starter-core' ), $singular ),
			'all_items'          => sprintf( _x( '%s', 'Admin bar ' . $singular . ' name', 'starter-core' ), $admin_bar_name ),
			'search_items'       => sprintf( __( 'Search %s', 'starter-core' ), $plural ),
			'parent_item_colon'  => sprintf( __( 'Parent %s:', 'starter-core' ), $plural ),
			'not_found'          => sprintf( __( 'No %s found.', 'starter-core' ), $plural ),
			'not_found_in_trash' => sprintf( __( 'No %s found in Trash.', 'starter-core' ), $plural ),
		);
	}

	public function post_type_args( $labels, $slug, $args = array() ) {
		$can_edit     = ( current_user_can( 'edit_posts' ) );
		$default_args = array(
			'labels'             => $labels,
			'public'             => $can_edit,
			'publicly_queryable' => $can_edit,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => $slug ),
			'capability_type'    => 'post',
			'has_archive'        => false,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title' ),
		);

		return wp_parse_args( $args, $default_args );
	}

}

new Register_Post_Types();
